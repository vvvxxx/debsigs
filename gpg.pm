#    debsigs: Package signing/verification system 
#    Copyright (C) 2000   Progeny Linux Systems, Inc. <jgoerzen@progeny.com>
#    Copyright (C) 2009   Peter Pentchev <roam@ringlet.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program; if not, write to the Free Software
#    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

package Debian::debsigs::gpg;

use strict;
use warnings;

use Debian::debsigs::forktools ':all';

our $VERSION = '1.06';

sub getkeyfromfd {
  my $forkfd = shift @_;

  my ($gpgfd, $gpgpid) = forkreader($forkfd,
					       "/usr/bin/gpg",
					       "--list-packets");
  
  my ($keyid, $date);

  my $line = <$gpgfd>;
  # print STDERR "gpg: got first line: $line";
  die 'bad gpg line' unless ($line =~ '^:signature packet:');
  ($keyid) = $line =~ /^:signature packet: .+ keyid ([0-9a-fA-F]+)/;
  die 'invalid gpg line' unless($keyid);
  while (defined($line = <$gpgfd>)) {
    # print STDERR "gpg: got line: $line";
    unless ($date) {
      ($date) = $line =~ /^\s+.*created ([0-9]+)/;
    }
  }
  
  assertsuccess($gpgpid, 'gpg --list-packets');
  # print STDERR "gpg: returning ($keyid, $date)\n";
  return ($keyid, $date);
}

sub getkeynamefromid {
  my $keyid = shift @_;

  my ($gpgfd, $gpgpid) = forkreader(undef, "/usr/bin/gpg",
					       "--list-keys", $keyid);
  
  my $line = <$gpgfd>;
  chomp $line;
  
  my ($name) = $line =~ m'pub\s+.+/[0-9A-Fa-f]+ \d\d\d\d-\d\d-\d\d (.+)$';
  return $name;
}

1;
