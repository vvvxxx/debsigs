package Debian::debsigs::debsigsmain;

use strict;
use warnings;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

require Exporter;
require AutoLoader;

@ISA = qw(Exporter AutoLoader);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
	
);
$VERSION = '0.01.19';


# Preloaded methods go here.

# Autoload methods go after =cut, and are processed by the autosplit program.

1;
__END__
# Below is the stub of documentation for your module. You better edit it!

=head1 NAME

Debian::debsigs::debsigsmain - Perl extension for signing Debian packages

=head1 SYNOPSIS

  use Debian::debsigs::debsigsmain;

=head1 DESCRIPTION

This extension is undocumented so far.  Sorry.  Patches welcome.

=head1 AUTHOR

John Goerzen for Progeny Linux Systems, Inc.

=head1 SEE ALSO

perl(1).

=cut
